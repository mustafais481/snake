# Snake Game in C#

## Table of Contents

- [Introduction](#introduction)
- [Architecture](#architecture)
  - [Couche Client (Presentation Layer)](#couche-client-presentation-layer)
  - [Couche Data (Data Layer)](#couche-data-data-layer)
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)

## Introduction

Ce projet est une implémentation du jeu classique Snake en C# utilisant Windows Presentation Foundation (WPF). Le projet est structuré selon une architecture en trois couches pour faciliter la maintenance et l'évolution du code : la couche client, la couche data et la couche serveur.

## Architecture

### Couche Client (Presentation Layer)

La couche client gère l'interface utilisateur et les interactions utilisateur. Elle est responsable du rendu graphique du jeu et de la gestion des entrées utilisateur.

**Fichier Principal : `MainWindow.xaml.cs`**

```csharp
namespace Snake
{
    public partial class MainWindow : Window
    {
        private readonly int rows = 15, cols = 15;
        private readonly Image[,] gridImages;
        private GameState gameState;
        private bool gameRunning;

        public MainWindow()
        {
            InitializeComponent();
            gridImages = SetUpGrid();
            gameState = new GameState(rows, cols);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (gameState.GameOver)
            {
                return;
            }

            switch (e.Key)
            {
                case Key.Left:
                    gameState.ChangeDirection(Direction.Left);
                    break;
                case Key.Right:
                    gameState.ChangeDirection(Direction.Right);
                    break;
                case Key.Up:
                    gameState.ChangeDirection(Direction.Up);
                    break;
                case Key.Down:
                    gameState.ChangeDirection(Direction.Down);
                    break;
            }
        }

        private Image[,] SetUpGrid()
        {
            Image[,] images = new Image[rows, cols];
            GameGrid.Rows = rows;
            GameGrid.Columns = cols;
            GameGrid.Width = GameGrid.Height * (cols / (double)rows);

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    Image image = new Image
                    {
                        Source = Images.Empty,
                        RenderTransformOrigin = new Point(0.5, 0.5)
                    };

                    images[r, c] = image;
                    GameGrid.Children.Add(image);
                }
            }

            return images;
        }
    }
}
```

### Couche Data (Data Layer)

La couche data gère l'état du jeu, y compris la grille, le score, et les positions du serpent. Elle est responsable de maintenir les données et de les rendre accessibles à la couche logique.

```csharp

namespace Snake
{
    public class GameState
    {
        public int Rows { get; }
        public int Cols { get; }
        public GridValue[,] Grid { get; }
        public Direction Dir { get; private set; }
        public int Score { get; private set; }
        public bool GameOver { get; private set; }

        private readonly LinkedList<Direction> dirChanges = new LinkedList<Direction>();
        private readonly LinkedList<Position> snakePositions = new LinkedList<Position>();
        private readonly Random random = new Random();

        public GameState(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            Grid = new GridValue[rows, cols];
            Dir = Direction.Right;

            AddSnake();
            AddFood();
        }

        private void AddSnake()
        {
            int r = Rows / 2;

            for (int c = 1; c <= 3; c++)
            {
                Grid[r, c] = GridValue.Snake;
                snakePositions.AddFirst(new Position(r, c));
            }
        }

        private void AddFood()
        {
            List<Position> empty = new List<Position>(EmptyPositions());

            if (empty.Count == 0) return;

            Position position = empty[random.Next(empty.Count)];
            Grid[position.Row, position.Col] = GridValue.Food;
        }

        private IEnumerable<Position> EmptyPositions()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Cols; c++)
                {
                    if (Grid[r, c] == GridValue.Empty)
                    {
                        yield return new Position(r, c);
                    }
                }
            }
        }
    }
}
```


## Installation

Clonez le dépôt :

```sh
git clone https://gitlab.com/mustafais481/snake.git
```


    Ouvrez le projet dans Visual Studio.
    Restaurez les packages NuGet si nécessaire.
    Compilez et exécutez le projet.

# Usage

Utilisez les touches fléchées pour diriger le serpent. Mangez la nourriture pour grandir et augmenter votre score. Évitez les murs et votre propre corps.

# Contributing
Les contributions sont les bienvenues ! Veuillez soumettre des pull requests ou ouvrir des issues pour toute suggestion ou amélioration.
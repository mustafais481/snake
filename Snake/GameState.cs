﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Snake
{
    public class GameState
    {
        public int Rows { get; }
        public int Cols { get; }
        public GridValue[,] Grid { get; }
        public Direction Dir { get; private set; }
        public int Score { get; private set; }
        public bool GameOver { get; private set; }

      
        private readonly LinkedList<Direction> dirChanges = new LinkedList<Direction>();
        private readonly LinkedList<Position> snakePositions = new LinkedList<Position>();
        private readonly Random random = new Random();
        private MediaPlayer eatSoundPlayer;
        private MediaPlayer deathSoundPlayer; 

        public GameState(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            Grid = new GridValue[rows, cols];
            Dir = Direction.Right;

            string eatSoundPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Assets", "eat.wav");
            eatSoundPlayer = new MediaPlayer();
            eatSoundPlayer.Open(new Uri(eatSoundPath));

            string deathSoundPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Assets", "death.wav");
            deathSoundPlayer = new MediaPlayer();
            deathSoundPlayer.Open(new Uri(deathSoundPath));

            AddSnake();
            AddFood();
        }

        private void AddSnake()
        {
            int r = Rows / 2;

            for (int c = 1; c <= 3; c++)
            {
                Grid[r, c] = GridValue.Snake;
                snakePositions.AddFirst(new Position(r, c));
            }
        }

        private IEnumerable<Position> EmptyPositions()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Cols; c++)
                {
                    if (Grid[r, c] == GridValue.Empty)
                    {
                        yield return new Position(r, c);
                    }
                }
            }
        }

        private void AddFood()
        {
            List<Position> empty = new List<Position>(EmptyPositions());

            if (empty.Count == 0) return;

            Position position = empty[random.Next(empty.Count)];
            Grid[position.Row, position.Col] = GridValue.Food;

        }

        public Position HeadPosition()
        {
            return snakePositions.First.Value;
        }

        public Position TailPosition()
        {
            return snakePositions.Last.Value;
        }

        public IEnumerable<Position> SnakePositions()
        {
            return snakePositions;
        }

        private void AddHead(Position position)
        {
            snakePositions.AddFirst(position);
            Grid[position.Row, position.Col] = GridValue.Snake;
        }

        private void RemoveTail()
        {
            Position tail = snakePositions.Last.Value;
            Grid[tail.Row, tail.Col] = GridValue.Empty;
            snakePositions.RemoveLast();
        }

        public void ChangeDirection(Direction direction)
        {
            if(CanChangeDirection(direction))
            {
                dirChanges.AddLast(direction);
            }
        }

        private bool OutSideGrid(Position position)
        {
            return position.Row < 0 || position.Row >= Rows || position.Col < 0 || position.Col >= Cols;
        }

        private GridValue WillHit(Position newHeadposition)
        {
            if(OutSideGrid(newHeadposition))
            {
                return GridValue.Outside;
            }

            if(newHeadposition == TailPosition())
            {
                return GridValue.Empty;
            }

            return Grid[newHeadposition.Row, newHeadposition.Col];
        }

        private Direction GetLastDirection()
        {
            if(dirChanges.Count == 0)
            {
                return Dir;
            }

            return dirChanges.Last.Value;
        }

        private bool CanChangeDirection(Direction newDir)
        {
            if(dirChanges.Count == 2)
            {
                return false;
            }

            Direction lastDir = GetLastDirection();

            return newDir != lastDir && newDir != lastDir.Opposite();
        }
        public void Move()
        {
            if(dirChanges.Count > 0)
            {
                Dir = dirChanges.First.Value;
                dirChanges.RemoveFirst();
            }

            Position newHeadPos = HeadPosition().Translate(Dir);
            GridValue hit = WillHit(newHeadPos);

            if(hit == GridValue.Outside || hit == GridValue.Snake)
            {
                GameOver = true;
                deathSoundPlayer.Stop();
                deathSoundPlayer.Play();
            }

            else if (hit == GridValue.Empty)
            {
                RemoveTail();
                AddHead(newHeadPos);
            }

            else if(hit == GridValue.Food)
            {
                AddHead(newHeadPos);
                Score++;
                AddFood();

                eatSoundPlayer.Stop();
                eatSoundPlayer.Play();
            }
        }
    }
}
